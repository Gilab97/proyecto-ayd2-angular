import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-mensaje-compra',
  templateUrl: './mensaje-compra.component.html',
  styleUrls: ['./mensaje-compra.component.css']
})
export class MensajeCompraComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<MensajeCompraComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  enviar(){
    this.dialogRef.close();
    this.openSnackBar('Mensaje Enviado','');
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  cancelar(){
    this.dialogRef.close();
  }

}
