import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MensajeCompraComponent } from './mensaje-compra.component';
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {of} from "rxjs";

describe('MensajeCompraComponent', () => {
  let component: MensajeCompraComponent;
  let fixture: ComponentFixture<MensajeCompraComponent>;

  let dialogSpy: jasmine.Spy;
  const dialogRefSpyObj = jasmine.createSpyObj({ afterClosed : of({}), close: null });
  dialogRefSpyObj.componentInstance = { body: '' }; // attach componentInstance to the spy object...


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MensajeCompraComponent ],
      providers: [MatDialogRef, { provide: MatDialog }]
    })
    .compileComponents();
  }));
});
