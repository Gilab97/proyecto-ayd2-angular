import { Component, OnInit } from '@angular/core';
import { AutenticacionService } from 'src/app/services/autenticacion.service';
import { IUser, IPerfil } from 'src/app/models/IUser';
import { DatePipe } from '@angular/common'

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  usuario : IPerfil = {nombres: '', apellidos: '', contrasena: '', correo_electronico: '', descripcion: '',
    fecha_nacimiento: '', idUsuario: 0, nombre_colonia: '', nombre_residencial: '', nombre_tipo_usuario: ''};

  constructor(private autenticacionService:AutenticacionService) { }

  ngOnInit(): void {
    if(this.autenticacionService.getUserInfo()){
      this.autenticacionService.getPerfil(this.autenticacionService.getUserInfo().idUsuario).subscribe(
        res=>{
          this.usuario=res;
          console.log(this.usuario);
        }
      );
    }
  }

}
