import { Component, OnInit } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  constructor(private chatService:ChatService) { }
  listaChat:any;

  ngOnInit(): void {
    this.chatService.getChats().subscribe(
      res=>{
        this.listaChat=res;
      }
    );
  }

}
