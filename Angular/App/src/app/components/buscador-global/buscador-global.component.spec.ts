import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { BuscadorGlobalComponent } from './buscador-global.component';

import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {FormsModule} from "@angular/forms";
import {MatDialogModule} from "@angular/material/dialog";

describe('BuscadorGlobalComponent', () => {
  let component: BuscadorGlobalComponent;
  let fixture: ComponentFixture<BuscadorGlobalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuscadorGlobalComponent ],
      imports: [HttpClientTestingModule, FormsModule, MatDialogModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuscadorGlobalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('search', () => {
    it('should search', fakeAsync(() => {
      component.buscadorGlobal = 'Jelouda'
      component.buscarGlobal()
      tick(50);
      expect(true).toBeTruthy();
    }));
  });
});
