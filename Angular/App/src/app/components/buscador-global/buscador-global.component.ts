import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { MensajeCompraComponent } from '../mensaje-compra/mensaje-compra.component';
import { MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-buscador-global',
  templateUrl: './buscador-global.component.html',
  styleUrls: ['./buscador-global.component.css']
})
export class BuscadorGlobalComponent implements OnInit {
  buscadorGlobal="";
  productos:any=[];
  constructor(private productoService:ProductService,private dialog:MatDialog) { }

  ngOnInit(): void {
  }

  buscarGlobal(){
    this.productoService.busquedaGlobal(this.buscadorGlobal).subscribe(
      res=>{
        this.productos=res;
        console.log(this.productos);
      },
      err=>console.log("error ",err)
    );
  }



  public enviarMensaje(nombre:string,usuariodestino:string){
    console.log("FUNCIONANDO ");
    const dialogRef = this.dialog.open(MensajeCompraComponent
      , { width: '350px',height: '275px',
        data: {'nombre':nombre,'usuariodestino':usuariodestino}
      });
  }

}
