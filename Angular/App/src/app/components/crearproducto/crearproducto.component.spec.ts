import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { CrearproductoComponent } from './crearproducto.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {FormsModule} from '@angular/forms';

describe('CrearproductoComponent', () => {
  let component: CrearproductoComponent;
  let fixture: ComponentFixture<CrearproductoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearproductoComponent],
      imports: [ HttpClientTestingModule, FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearproductoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('login', () => {
    it('should login', fakeAsync(() => {
      component.guardarProducto()
      tick(50);
      expect(true).toBeTruthy();
    }));
  });

  describe('login', () => {
    it('should login', fakeAsync(() => {
      component.setInfo({text: 'asdf'})
      tick(50);
      expect(true).toBeTruthy();
    }));
  });
});
