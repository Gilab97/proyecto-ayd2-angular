import { Component, OnInit } from '@angular/core';
import { IProducto } from 'src/app/models/IProducto';
import { ProductService } from 'src/app/services/product.service';
import { AutenticacionService } from 'src/app/services/autenticacion.service';

@Component({
  selector: 'app-crearproducto',
  templateUrl: './crearproducto.component.html',
  styleUrls: ['./crearproducto.component.css']
})
export class CrearproductoComponent implements OnInit {
  producto:IProducto={};
  mensaje="";
  constructor(private productoService:ProductService,private usuarioService:AutenticacionService) { }

  ngOnInit(): void {
    if(this.usuarioService.getUserInfo()){
      this.producto.idUsuario=this.usuarioService.getUserInfo().idUsuario;
      console.log("USER ",this.producto.idUsuario);
    }
  }

  guardarProducto(){
    const d = this.usuarioService.getUserInfo()
    this.producto.idUsuario= d ? d.idUsuario : 0;
    this.productoService.crearProducto(this.producto).subscribe(
      (res:any)=>{
        this.setInfo(res)
      },
      err=>console.log(err)
    );
  }

  setInfo(res){
    this.mensaje=res.text;
    this.producto={};
    this.producto.idUsuario=this.usuarioService.getUserInfo().idUsuario;
  }

}
