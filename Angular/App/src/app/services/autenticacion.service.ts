import { Injectable, NgZone, Output } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { IUser, IPerfil } from '../models/IUser';
import { tap } from 'rxjs/operators';
import { isNullOrUndefined} from 'util';


@Injectable({
  providedIn: 'root'
})
export class AutenticacionService {

    API_URL='http://34.71.79.162:3000/api';
    constructor(private http:HttpClient) { }


    login(usuario:string,pass:string) :Observable<any>{

      let login_var={
        correo:usuario,
        password:pass
      }
      return this.http.post<any>(`${this.API_URL}/login`,login_var)
      .pipe(tap(
        (res:any)=>{
          if(res.res==false){
          }else{
            this.saveLocalStorageUser(res);
          }
        }
      ));
    }

    registro(usuario:IUser){
      return this.http.post(`${this.API_URL}/user/create-user`,usuario);
    }

    saveLocalStorageUser(user){
      localStorage.setItem("username",user.correo_electronico);
      let user_json=JSON.stringify(user);
      localStorage.setItem("CurrentUser",user_json);
    }

    public getUserInfo():IUser{
      let user=localStorage.getItem("CurrentUser");
      if(!isNullOrUndefined(user)){
        return JSON.parse(user);
      }
    }


    getPerfil(id):Observable<IPerfil>{
      return this.http.get<IPerfil>(`${this.API_URL}/user/perfil/${id}`);
    }

    

}
