import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AutenticacionService } from './autenticacion.service';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(private http:HttpClient,private userService:AutenticacionService) { }

  API_URL='http://34.71.79.162:3000/api';

  getChats():Observable<any>{
    const usuario=this.userService.getUserInfo();
    let id_usuario = 0
    if(usuario) { id_usuario = usuario.idUsuario }
    return this.http.get(`${this.API_URL}/user/give-chat/${id_usuario}`);
  }
}
