import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IProducto } from '../models/IProducto';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  API_URL="http://34.71.79.162:3000/api";
  constructor(private http:HttpClient) { }

  crearProducto(producto:IProducto){
    return this.http.post(`${this.API_URL}/product/createProduct`,producto);
  }

  busquedaGlobal(cadena:string){
    return this.http.get(`${this.API_URL}/product/busquedaGlobal/${cadena}`);
  }
}
