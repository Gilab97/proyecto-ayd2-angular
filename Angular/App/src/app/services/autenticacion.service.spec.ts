import {fakeAsync, TestBed, tick} from '@angular/core/testing';

import { AutenticacionService } from './autenticacion.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('AutenticacionService', () => {

  let controller: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  it('should be created', () => {
    controller = TestBed.get(HttpTestingController);
    const service: AutenticacionService = TestBed.get(AutenticacionService);
    expect(service).toBeTruthy();
  });

  describe('profile', () => {
    it('should get profile', fakeAsync(() => {
      const service: AutenticacionService = TestBed.get(AutenticacionService);
      service.getPerfil(20)
      tick(50);
      expect(true).toBeTruthy();
    }));
  });

  describe('user register', () => {
    it('should get register', fakeAsync(() => {
      const service: AutenticacionService = TestBed.get(AutenticacionService);
      service.registro({apellidos: "", contrasena: '', correo_electronico: '', descripcion: '', fecha_nacimiento: '',
        id_colonia: 0, idUsuario: 0, nombres: '', tipo_usuario: 0})
      tick(50);
      expect(true).toBeTruthy();
    }));
  });

  describe('localstorage', () => {
    it('should save', fakeAsync(() => {
      const service: AutenticacionService = TestBed.get(AutenticacionService);
      service.saveLocalStorageUser({apellidos: "", contrasena: '', correo_electronico: '', descripcion: '', fecha_nacimiento: '',
        id_colonia: 0, idUsuario: 0, nombres: '', tipo_usuario: 0})
      tick(50);
      expect(true).toBeTruthy();
    }));
  });
});
