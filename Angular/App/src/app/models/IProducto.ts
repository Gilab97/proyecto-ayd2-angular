export interface IProducto{
    idProducto?: Number,
    Nombre?: string, 
    descripcion?: string, 
    precio?: Number,
    idUsuario?: Number, 
    imagen?: string
}