export interface IUser{
    idUsuario?: number;
    nombres: string;
    apellidos: string;
    fecha_nacimiento: string;
    correo_electronico: string;
    contrasena: string;
    descripcion: string;
    tipo_usuario: number;
    id_colonia: number;
}

export interface IPerfil{
    idUsuario?: number;
    nombres: string;
    apellidos: string;
    fecha_nacimiento: string;
    correo_electronico: string;
    contrasena: string;
    descripcion: string;
    nombre_tipo_usuario: string;
    nombre_colonia: string;
    nombre_residencial: string;
}